module.exports = message => {
	if (en_cours == "true") {
		if (dead_folks.includes(message.author.id) == false) {
			if (message.channel.type == "group" || message.channel.type == "dm") {
				if (isOdd() == 1) {
					if (message.content != "lgaction" || message.content != "lgaction ") {
						interaction = message.content.slice(message.content.indexOf(" ") + 1)
						if (have_joined_alias.includes(interaction)) {
							execution()
						} else {
							message.reply("vous tentez d'agir avec quelqu'un qui n'existe pas (ou n'est pas dans la partie)...")
						}
					} else {
						message.reply("vous devez préciser avec qui agir !")
					}
				} else {
					message.reply("vous ne pouvez agir que pendant la nuit !")
				}
			} else {
				message.reply("quel que soit votre rôle, vous ne pouvez agir qu'en message privé !")
			}
		} else {
			message.reply("en tant que personne morte, vous n'avez aucun pouvoir sur le monde des réels...")
		}
	} else {
		message.reply("vous ne pouvez pas agir si la partie n'a pas encore commencée !")
	}
	
	function execution() {
		switch(roles[have_joined.indexOf(message.author.id)]) {
			case 0:
				if (part == 1) {
					if (dead_folks.includes(interaction) == false) {
						if (roles[have_joined_alias.indexOf(interaction)] != 0) {
							lg_vote[have_joined.indexOf(message.author.id)] = have_joined_alias.indexOf(interaction)
							message.reply(`vous avez voté pour ${interaction}.`)
						} else {
							message.reply("vous ne pouvez pas utiliser votre vote contre un Loup-Garou !")
						}
					} else {
						message.reply("vous ne pouvez pas voter pour éliminer quelqu'un qui est déjà mort !")
					}
				} else {
					message.reply("ce n'est pas encore votre tour !")
				}
				break
			case 1:
				message.reply("en tant que Simple Villageois, vous ne pouvez rien faire...")
				break
			case 2:
				if (part == 1) {
					if (dead_folks.includes(interaction) == false) {
						if (voyante_pouvoir_utilise == "false") {
							voyante_pouvoir_utilise = "true"
							voyante_role_brut = roles[have_joined_alias.indexOf(interaction)]
							message.reply("le vrai rôle de " + interaction + " est celui de " + nom_role() + "...")
							game_channel.send("La Voyante a vu qu'un joueur avait en fait le rôle de " + nom_role() + " !")
						} else {
							message.reply("votre pouvoir ne peut être utilisé qu'une fois par nuit !")
						}
					} else {
						message.reply("vous ne pouvez pas utiliser votre pouvoir sur les morts...")
					}
				} else {
					message.reply("ce n'est pas encore votre tour !")
				}
				break
			case 3:
				message.reply("en tant que Petite Fille, vous ne pouvez rien faire... à part écouter, bien sûr.")
				break
			case 4:
				if (part == 2) {
					if (sorciere_vie_utilise == "false" || sorciere_mort_utilise == "false") {
						if (sorciere_vie_utilise == "false") {
							message.reply(`Ok, alors, la victime des Loups-Garous de cette nuit est placeholder.
Faîtes "vis" avant la fin de votre tour afin d'utiliser votre potion de vie sur cette personne.`)
						}
						if (sorciere_mort_utilise == "false") {
							message.reply(`Vous pouvez utiliser votre potion de mort sur n'importe quel vivant. Faîtes "meurs [personne]" afin de tuer la personne de votre choix.
(sans les [])`)
						}
					} else {
						message.reply("vous avez déjà utilisé vos deux potions !")
					}
				} else {
					message.reply("ce n'est pas encore votre tour !")
				}
				break
			default:
				message.reply("ok boomer")
				break
		}
	}
}

function isOdd() {return tour % 2}

function nom_role() {
	switch(voyante_role_brut) {
		case 0:
			return "Loup-Garou"
			break
		case 1:
			return "Simple Villageois"
			break
		case 2:
			return "Voyante"
			break
		case 3:
			return "Petite Fille"
			break
		case 4:
			return "Sorcière"
			break
		case 5:
			return "Chasseur"
			break
		case 6:
			return "Cupidon"
			break
		case 7:
			return "Salvateur"
			break
		default:
			instructions = "Vous n'êtes pas censé avoir ce rôle. Reportez ce bug à Istérix#1287, merci ! <3"
			return "ERREUR: Rôle non-existant"
			break
	}
}
