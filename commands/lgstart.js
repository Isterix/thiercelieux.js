module.exports = message => {
	if (en_cours == "false") {
		if (starting != "true") {
			starting = "true"
			game_channel = message.channel
			instructions = "Vous n'êtes pas censé voir ce placeholder. Reportez ce souci à Istérix#1287, merci ! <3"
			message.reply("la partie commencera dans " + (countdown - 5) + " secondes !")
			minus_one()
		} else {
			message.reply("la partie est déjà en train de commencer...")
		}
	} else {
		message.reply("la partie est déjà en cours...")
	}
}

function minus_one() {
	countdown -= 5
	if (countdown > 0 && car_allowed == "true") {
		setTimeout(minus_one, 5000)
		client.user.setActivity(String(countdown) + " secondes avant le commencement de la partie !")
		.catch(console.error)
	} else if (countdown <= 0) {
		if (have_joined.length >= 1) {
			paternes()
			countdown = 65
			en_cours = "true"
			var x
			role_order = 0
			for (x of have_joined) {
				function rolling() {
					role = Math.floor((Math.random() * 8) + 0)
					countRoles()
					if (role_appears >= paterne[role]) {
						rolling()
					} else {
						roles[role_order] = role
						dm_who_joined[role_order].send("Vous avez le rôle suivant: " + nom_role())
						dm_who_joined[role_order].send(instructions)
						role_order ++
						sorciere_vie_utilise = "false"
						sorciere_mort_utilise = "false"
						tour = 1
						part = 0
						console.log(roles)
					}
				}
				rolling()
			}
			playing()
		} else {
			game_channel.send("Pas assez de joueurs pour commencer la partie...")
			en_cours = "false"
			countdown = 65
		}
	} else {
		car_allowed = "true"
	}
}

function countRoles() {
	role_appears = 0
	for (var i = 0; i < roles.length; i ++) {
		if (roles[i] === role) {
			role_appears ++
		}
	}
}

function paternes() {
	switch(have_joined.length) {
		case 4:
			paterne = [1, 1, 1, 0, 0, 0, 0, 1]
			break
		case 5:
		case 6:
			paterne = [1, 2, 1, 0, 1, 0, 0, 1]
			break
		case 7:
			paterne = [2, 0, 1, 1, 1, 1, 0, 1]
			break
		case 8:
		case 9:
			paterne = [2, 1, 1, 1, 1, 1, 1, 1]
			break
		default:
			paterne = [30, 30, 1, 1, 1, 1, 1, 1]
			break
	}
}

function nom_role() {
	switch(role) {
		case 0:
			instructions = `Faîtes en sorte de survivre, car les villageois veulent votre mort !
La nuit, réveillez-vous sous votre forme de Loup-Garou et mangez (ou non) un villageois, de préférence quelqu'un qui peut vous suspecter.
Le jour, réveillez-vous sous votre forme humaine et tentez de convaincre les villageois qu'un des leurs est un Loup-Garou afin qu'ils l'éliminent.`
			return "Loup-Garou"
			break
		case 1:
			instructions = `Faîtes en sorte de survivre, car les Loups-Garous tenteront de vous manger la nuit et de vous éliminer le jour !
En tant que Simple Villageois, vous ne pouvez utiliser que votre déduction afin de connaître la véritable identité de vos compatriotes.
Vous ne pouvez rien faire la nuit. Par contre, le jour, utilisez votre seul vote contre un villageois que vous pensez être un Loup-Garou.`
			return "Simple Villageois"
			break
		case 2:
			instructions = `Faîtes en sorte la nuit d'utiliser votre rôle au maximum de son potentiel !
Chaque nuit, vous pouvez découvrir le véritable rôle du joueur de votre choix. Le rôle du joueur sera révélé au public, mais pas le nom du joueur.
Parce que vous pouvez guider les villageois à voter contre les Loups-Garous, ces derniers vont certainement faire de vous leur priorité...`
			return "Voyante"
			break
		case 3:
			instructions = `Faîtes en sorte la nuit d'écouter ce que les Loups-Garous se disent entre eux en toute discrétion !
Vous vous réveillez chaque nuit afin d'écouter les Loups-Garous. Le jour, vous pouvez voter avec les autres adultes.
Si les Loups-Garous se rendent compte que vous détenez des informations trop confidentielles, votre vie risque d'être en danger la nuit...`
			return "Petite Fille"
			break
		case 4:
			instructions = `Faîtes en sorte d'utiliser vos pouvoirs de vie et de mort au bon moment au bon endroit !
La nuit, vous pouvez utiliser votre potion de vie afin de sauver la victime des Loups-Garous. Vous pouvez aussi utiliser votre potion de mort afin de tuer quelqu'un vous-même.
Attention à sauver les personnes les plus importantes et non pas les tuer, ça risquerait sinon d'être embarassant...`
			return "Sorcière"
			break
		case 5:
			instructions = `Faîtes en sorte de survivre jusqu'à ce que vous arrivez à une conclusion !
En tant que Chasseur, vous êtes identique aux Simples Vllageois jusqu'à votre mort, où vous pourrez tuer la personne de votre choix.
Déduisez bien quels villageois sont en réalité des loups et vite, vous ne voulez pas tirer au hasard...`
			return "Chasseur"
			break
		case 6:
			instructions = `Faîtes en sorte de survivre, enfin, si vous le voulez !
Après la première nuit, vous êtes aussi inutile qu'un Simple Villageois. Mais pendant la première nuit, vous ajoutez du piment (de l'aléatoire) à la partie.
Mettez en couple deux joueurs de votre choix, dont vous-même si vous le souhaitez. Si l'un d'eux meurt, l'autre se suicidera...`
			return "Cupidon"
			break
		case 7:
			instructions = `Faîtes en sorte de prédire quelle sera la prochaine cible des Loups-Garous chaque nuit !
La nuit, vous vous réveillez afin de sauver la personne que vous voulez de la prochaine attaque des Loups-Garous, vous-même inclus.
Par contre, vous ne pouvez pas protéger deux fois de suite la même personne. Choisissez bien...`
			return "Salvateur"
			break
		default:
			instructions = "Vous n'êtes pas censé avoir ce rôle. Reportez ce bug à Istérix#1287, merci ! <3"
			return "Erm..."
			break
	}
}

function isOdd() {return tour % 2}

function cupidon() {
	cupidon_time -= 5
	if (stopping == "true") {
		stoping()
	} else if (cupidon_time <= 0) {
		part = 1
		playing()
	} else {
		setTimeout(cupidon, 5000)
		client.user.setActivity("Le Cupidon a encore " + String(cupidon_time) + " secondes.")
	}
}

function lgandco() {
	lg_time -= 5
	if (stopping == "true") {
		stoping()
	} else if (lg_time <= 0) {
		part = 2
		playing()
	} else {
		setTimeout(lgandco, 5000)
		client.user.setActivity("Plus que " + String(lg_time) + " secondes avant la fin du tour des LGs, de la Vovo, de la PF et du Salva.")
	}
}

function sorciere() {
	sorciere_time -= 5
	if (stopping == "true") {
		stoping()
	} else if (sorciere_time <= 0) {
		part = 3
		playing()
	} else {
		setTimeout(sorciere, 5000)
		client.user.setActivity("Il reste " + String(sorciere_time) + " secondes avant la fin du tour de la sorcière.")
	}
}

function stoping() {
	stopping = "false"
	en_cours = "false"
	dm_who_joined = []
	have_joined = []
	have_joined_alias = []
	has_voted_who = []
	roles = []
	dead_folks = []
	potentially_dead = []
	lg_vote = []
	game_channel.send("Partie arrêtée.")
}

function nighting() {
	if (stopping == "true") {
		stoping()
	} else if (part == 0) {
		cupidon_time = 45
		game_channel.send(`Comme c'est la première nuit, le Cupidon se réveille !
Il a ${cupidon_time} secondes pour agir.`)
		setTimeout(cupidon, 5000)
	} else if (part == 1) {
		lg_time = 60
		game_channel.send(`Loups-Garous, Voyante, Petite Fille et Salvateur, c'est votre heure d'agir !
Vous avez ${lg_time} secondes pour m'envoyer en message privé ce que vous voulez faire.`)
		setTimeout(lgandco, 5000)
	} else if (part == 2) {
		sorciere_time = 45
		game_channel.send("La sorcière peut maintenant agir !")
		setTimeout(sorciere, 5000)
	} else {
		tour ++
		playing()
	}
}

function day() {
	day_countdown -= 5
	client.user.setActivity("La journée se termine dans " + String(day_countdown) + " secondes.")
	if (stopping == "true") {
		stoping()
	} else if (day_countdown <= 0) {
		tour ++
		part = 1
		checked = []
		checking = have_joined.length
		voting()
		nighting()
	} else {
		setTimeout(day, 5000)
	}
}

function playing() {
	if (isOdd() == 1) {
		voyante_pouvoir_utilise = "false"
		nighting()
		if (part <= 2) {return}
	} else if (isOdd() == 0) {
		day_countdown = 180
		game_channel.send("C'est le jour.")
		setTimeout(day, 5000)
	} else {game_channel.send("ERREUR 'PAIR/IMPAIR'; Parlez de ce bug à Istérix#1287.")}
}

function voting() {
	checked[checking] = has_voted_who.filter(i => i === checking).length
	console.log(checked[checking])
	checking --
	if (checking < -1) {
		console.log(checking)
		console.log(checked)
	} else {
		voting()
	}
}
