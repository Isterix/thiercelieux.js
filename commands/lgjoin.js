module.exports = message => {
	if (en_cours == "false") {
		if (have_joined.includes(message.author.id)) {
			message.reply("vous avez déjà rejoint la partie.")
		} else {
			dm_who_joined.push(message.author)
			have_joined.push(message.author.id)
			have_joined_alias[have_joined.indexOf(message.author.id)] = message.author.username
			has_voted_who[have_joined.indexOf(message.author.id)] = -1
			message.react('✅')
			.catch(error => {
				console.error("Failed to react to lgjoin: " + error)
				message.reply("vous avez rejoint la partie avec succès !")
			})
			console.log(have_joined)
		}
	} else {
		message.reply("vous ne pouvez pas rejoindre une partie en cours !")
		if (have_joined.includes(message.author.id)) {
			message.reply("au fait, vous avez déjà rejoint la partie.")
		}
	}
}
