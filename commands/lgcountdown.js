module.exports = message => {
	try {
		countdown = message.toString().slice(12)
		if (countdown == "") {message.reply("utilisez un argument avec cette commande !")} else {
			if (countdown.includes(".")) {countdown = countdown.slice(0, countdown.indexOf("."))}
			if (countdown.includes("-")) {countdown = countdown.replace("-", "")}
			countdown = Number(countdown)
			if (Number.isNaN(countdown)) {
				countdown = 65
				message.reply("mauvais usage de la commande. N'utilisez pas de lettres après celle-ci. Compte à rebours réinitialisé à 60 secondes.")
			} else {
				message.reply("il faut désormais " + countdown + " secondes pour qu'une partie se lance.")
				if (starting == "false") {countdown += 5}
			}
		}
	} catch(err) {
		console.log("lgcountdown.js: " + err)
		message.reply("mauvais usage de la commande. Utilisez la commande suivi d'un espace puis d'un nombre entier.")
	}
}
