module.exports = message => {
	if (have_joined.includes(message.author.id)) {
		if (en_cours == "false") {
			dm_who_joined.splice(have_joined.indexOf(message.author.id), 1)
			have_joined.splice(have_joined.indexOf(message.author.id), 1)
			have_joined_alias.splice(have_joined.indexOf(message.author.id), 1)
			has_voted_who.splice(have_joined.indexOf(message.author.id), 1)
			message.reply("vous ne jouerez pas lors de la prochaine partie.")
		} else {
			game_channel.send(message.author.username + " a quitté la partie.")
			dm_who_joined.splice(have_joined.indexOf(message.author.id), 1)
			have_joined.splice(have_joined.indexOf(message.author.id), 1)
			have_joined_alias.splice(have_joined.indexOf(message.author.id), 1)
			has_voted_who.splice(have_joined.indexOf(message.author.id), 1)
		}
	} else {
		message.reply("vous n'avez pas rejoint de partie, donc vous ne pouvez pas en quitter une.")
	}
}
