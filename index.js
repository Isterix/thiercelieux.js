require('dotenv').config()
const fs = require('fs')
const Discord = require('discord.js')
client = new Discord.Client()
client.login(process.env.BOT_TOKEN)
en_cours = "false"
car_allowed = "true"
countdown = 65
tour = 0
stopping = "false"
starting = "false"

dm_who_joined = []
have_joined = []
have_joined_alias = []
has_voted_who = []
roles = []

dead_folks = []
potentially_dead = []
lg_vote = []

fs.readdir('./events/', (err, files) => {
	files.forEach(file => {
		const eventHandler = require(`./events/${file}`)
		const eventName = file.split('.')[0]
		client.on(eventName, (...args) => eventHandler(client, ...args))
	})
})
