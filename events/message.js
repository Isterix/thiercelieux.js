const lgstart = require('../commands/lgstart')
const lgcountdown = require('../commands/lgcountdown')
const lgstop = require('../commands/lgstop')
const lgjoin = require('../commands/lgjoin')
const lgleave = require('../commands/lgleave')
const lgvote = require('../commands/lgvote')
const lgaction = require('../commands/lgaction')
const vis = require('../commands/vis')
const meurs = require('../commands/meurs')

module.exports = (client, message) => {
	if (message.content.startsWith('lgstart')) {
		return lgstart(message)
	}
	if (message.content.startsWith('lgcountdown')) {
		return lgcountdown(message)
	}
	if (message.content.startsWith('lgstop')) {
		return lgstop(message)
	}
	if (message.content.startsWith('lgjoin')) {
		return lgjoin(message)
	}
	if (message.content.startsWith('lgleave')) {
		return lgleave(message)
	}
	if (message.content.startsWith('lgvote')) {
		return lgvote(message)
	}
	if (message.content.startsWith('lgaction')) {
		return lgaction(message)
	}
	if (message.content.startsWith('vis')) {
		return vis(message)
	}
	if (message.content.startsWith('meurs')) {
		return meurs(message)
	}
}
